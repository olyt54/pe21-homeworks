"use strict"

function addList(sourceArr) {

    const newList = document.createElement("ul");
    const itemsList = sourceArr.map(item => {
        if (Array.isArray(item)) {
            addList(item);
        } else {
            return `<li>${item}</li>`;
        }
    });
    itemsList.forEach(item => {
        return newList.insertAdjacentHTML("beforeend", item);
    });
    console.log(itemsList);
    document.body.prepend(newList);
}

addList([1, [1, 2, [3, 2, 1]], 3, 4]);


