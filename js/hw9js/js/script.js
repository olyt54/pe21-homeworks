"use strict"

function showTab() {
    const tabHeadsContainer = document.getElementsByClassName("tabs")[0],
        allTabsHeads = [...document.getElementsByClassName("tabs-title")],
        allTabContent = [...document.querySelectorAll(".tabs-content > li")];

    allTabContent.forEach((item, index) => {
        item.hidden = !(allTabsHeads[index].classList.contains("active"));
    });

    tabHeadsContainer.addEventListener("click", e => {
        const tabHeadSelected = e.target,
            previousActiveTab = allTabsHeads.find(item => {
                return item.classList.contains("active");
            });

        previousActiveTab.classList.remove("active");
        tabHeadSelected.classList.add("active");

        allTabContent.forEach((item, index) => {
            item.hidden = !(allTabsHeads[index].classList.contains("active"));
        });
    })
}
showTab();

// я пробовал разные варианты с dataset, но чот оно у меня полноценно так и не заработало
// основная проблема была - что нужно изначально показать правильную лишку
// этот вариант, наверное, немного топорный и типа может дурацкий
// но это единственное из написанного мной, что в, данном случае, полноценно и корректно работалo:)