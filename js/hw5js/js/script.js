"use strict"

function createNewUser() {
    let firstName = prompt("Enter your first name", "");
    let lastName = prompt("Enter your last name", ""),
        birthday = prompt("Enter date of your birthday in dd.mm.yyyy format", "");
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin: function (firstName, lastName) {
            return this.login = firstName[0].toLowerCase() + lastName.toLowerCase();
        },
        getAge: function () {
            let currentDate = new Date(),
                userTemporaryBirthday = birthday.split("."),
                userBirthday = new Date(userTemporaryBirthday[1] + "." + userTemporaryBirthday[0] + "." + userTemporaryBirthday[2]);
            if (currentDate.getDate() < userBirthday.getDate() && currentDate.getMonth() <= userBirthday.getMonth()) {
                this.age = currentDate.getFullYear() - userBirthday.getFullYear() - 1;
            } else {
                this.age = currentDate.getFullYear() - userBirthday.getFullYear();
            }
            return this.age;
        },
        getPassword: function () {
            return this.password = firstName[0].toUpperCase() + lastName.toLowerCase() + birthday.slice(6);

        },
    }
    Object.defineProperties(newUser, firstName, {
        writable: false,
        set: function setFirstName (newFirstName) {
            return this.firstName = newFirstName;
        }
    })
    Object.defineProperties(newUser, lastName, {
        writable: false,
        set: function setLastName (newLastName) {
            return this.lastName = newLastName;
        }
    })
    return newUser;
}
const newUser = createNewUser()
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

// Ответ на вопрос:
// Экранирование - способ использовать зарезервированный синтаксис джс внутри строки, ну например,
// самое простое - написать что-либо в скобках внутри строки
//
// А ещё я запутался в том как правильно создать обьект newUser - внутри функции, либо переменой в значение
// которой записывается функция. потому что это странно, функция должна создать обьект и вернуть его
// но если законсолить функцию, то выходит, что созданный обьект с именем newUser виден только внутри функции
// и выдает андефайнд в консоли
// в общем я вроде понимаю, но не сильно "гарольд.джпег"

