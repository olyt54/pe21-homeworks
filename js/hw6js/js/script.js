"use strict";

function filterBy(arr, dataType) {
    const filteredArr = [];
    arr.forEach((element) => {
        if (typeof(element) !== dataType
        || element !== null && dataType !== null) {
            filteredArr.push(element);
        }
    })
    return filteredArr;
}


// Цикл forEach для каждого элемента массива вызывает функцию, которая уже будет выполнять
// с этим элементом какие-то действия

