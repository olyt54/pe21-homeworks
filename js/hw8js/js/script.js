"use strict"

const mainContainer = document.createElement("div");
mainContainer.classList.add("container");

function createCloseBtn(inputName, priceContainer) {
    const closeBtn = document.createElement("button"),
        closeFirstLine = document.createElement("span"),
        closeSecondLine = document.createElement("span");

    closeBtn.classList.add("close-btn");
    closeFirstLine.classList.add("line", "first");
    closeSecondLine.classList.add("line", "second");

    closeBtn.appendChild(closeFirstLine);
    closeBtn.appendChild(closeSecondLine);

    closeBtn.addEventListener("click", () => {
        inputName.value = null;
        mainContainer.removeChild(priceContainer);
    })

    return closeBtn;
}

function createPriceContainer(inputName) {
    const priceSpanContainer = document.createElement("div"),
        priceSpan = document.createElement("span");

    priceSpanContainer.classList.add("price-string-container");
    priceSpan.classList.add("price-string");

    priceSpan.textContent = `Текущая цена ${inputName.value}`;

    priceSpanContainer.appendChild(priceSpan);
    priceSpanContainer.appendChild(createCloseBtn(inputName, priceSpanContainer));

    return priceSpanContainer;
}

function createErrorMessage() {
    const errorMessage = document.createElement("span");

    errorMessage.textContent = "Please enter correct price";

    errorMessage.dataset.type = "error";

    return errorMessage;
}

function createInput () {
    const priceLabel = document.createElement("label"),
        inputName = document.createElement("span"),
        priceInput = document.createElement("input");

    priceLabel.classList.add("price-label");
    inputName.classList.add("input-name");
    priceInput.classList.add("price-input");

    inputName.textContent = "Price";

    priceLabel.appendChild(inputName);
    priceLabel.appendChild(priceInput);

    priceInput.addEventListener("focus", () => {
    priceInput.classList.add("focus");
    })

    priceInput.addEventListener("focusout", () => {
        if (priceInput.value < 0) {
            priceInput.classList.add("incorrect");
            mainContainer.appendChild(createErrorMessage());
        } else if (priceInput.value === "" || isNaN(priceInput.value)) {
            priceInput.classList.remove("focus");
            priceInput.classList.remove("incorrect");
        } else {
            mainContainer.insertBefore(createPriceContainer(priceInput), priceLabel);
            [...mainContainer.children].find(item => {
                return item.dataset.type;
            }).remove();
            if (priceInput.classList.contains("incorrect")) {
                priceInput.classList.remove("incorrect");
                priceInput.classList.add("focus");
            }
        }
    })
    return priceLabel;
}

mainContainer.appendChild(createInput());

document.body.prepend(mainContainer);


// обработчик события - это функция, которая не имеет возвращаемого результата и
// описывает действия, которые будут выполнены при совершении события, описанном
// его назначении



