"use strict"

document.location.hash = '';

$(document).ready(function() {
    const btn = $(".up-btn");

    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            btn.addClass('up-btn-show');
        } else {
            btn.removeClass('up-btn-show');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
    });
});

// $(window).scroll(function() {
//     if ($(this).scrollTop() > 100) {
//         if ($('.up-btn').is(':hidden')) {
//             $('.up-btn').css({opacity : 1}).fadeIn('slow');
//         }
//     } else { $('.up-btn').stop(true, false).fadeOut('fast'); }
// });
// $('.up-btn').click(function() {
//     $('html, body').stop().animate({scrollTop : 0}, 300);
// });

function slideSection() {
    const slideBtn = document.getElementsByClassName("toggle-btn")[0],
        slideContent = $(".hot-news-container");

    slideBtn.addEventListener("click", event => {
        slideContent.slideToggle(500);
    });
}

slideSection();

