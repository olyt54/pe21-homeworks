"use strict"

function highlightKey() {
    const btn = document.getElementsByClassName("btn");

    document.addEventListener("keydown", event => {
        console.log(event);
        [...btn].forEach(item => {
            if (item.dataset.key === event.key.toLowerCase()) {
                item.classList.toggle("pressed");
            } else if (item.dataset.key !== event.key.toLowerCase() && item.classList.contains("pressed")) {
                item.classList.remove("pressed");
            }
        })
    })
}

highlightKey();

// честно говоря я не нашёл внятного ответа на этот вопрос
// но мне кажется, что это связано с поведением клавиш 'по умолчанию'
// и для такого события на инпут пришлось бы писать очень громоздкие фильтры
// чтоб отсечь нежелательные срабатывания