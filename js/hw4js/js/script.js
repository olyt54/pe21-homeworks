"use strict"

function createNewUser() {
    let firstName = prompt("Enter your first name", "");
    let lastName = prompt("Enter your last name", "");
    const newUser = {
        firstName: firstName,
        lastName: lastName,
    }
    Object.defineProperties(newUser, firstName, {
        writable: false,
        set: function setFirstName (newFirstName) {
            return this.firstName = newFirstName;
        }
    })
    Object.defineProperties(newUser, lastName, {
        writable: false,
        set: function setLastName (newLastName) {
            return this.lastName = newLastName;
        }
    })
    return newUser;
}
createNewUser();

newUser.getLogin = function (firstName, lastName) {
    return this.login = name[0].toLowerCase() + lastName.toLowerCase();
}

console.log(newUser.getLogin(newUser.firstName, newUser.lastName));

// в тз не написано как именно функция должна возвращать новое имя, потому ввверху был вариант с аргументом
// а тут в коментах с промптом
//
// Object.defineProperties(newUser, firstName, {
//     writable: false,
//     set: function setFirstName () {
//         return this.firstName = prompt("Enter your new first name", "");
//     }
// })
// Object.defineProperties(newUser, lastName, {
//     writable: false,
//     set: function setLastName () {
//         return this.lastName = prompt("Enter your new last name", "");
//     }
// })
//
// ответ на вопрос:
// метод обьекта - это функция записанная в значение свойства, выполняющая какие-либо операции с данными
// в этом обьекте