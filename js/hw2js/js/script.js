"use strict"

let userNumber = +prompt("Enter your number", "");
while (!Number.isInteger(userNumber)) {
    userNumber = +prompt("Enter integer", userNumber);
}

// switch (userNumber) {
//     case userNumber >= 5:
//         for (let i = 0; i <= userNumber; i++) {
//             if (i % 5 === 0) {
//                 console.log(i);
//             }
//         }
//         break;
//     case userNumber < 5:
//         console.log("Sorry, no numbers");
//         break;
// }

if (userNumber >= 5) {
    for (let i = 0; i <= userNumber; i++) {
        if (i % 5 === 0) {
            console.log(i);
        }
    }
} else {
    console.log("Sorry, no numbers");
}

let m = +prompt("Enter lowest number", ""),
    n = +prompt("Enter greater number", "");

while (n <= m) {
    alert("Error. Wrong numbers")
    m = +prompt("Must be lesser than n, try again", m);
    n = +prompt("Must be bigger than m, try again", n);
}
pimpMyLoop:
for (let i = m; i <= n; i++) {
    for (let j = m; j < i; j++) {
        if (i % j === 0) continue pimpMyLoop;
    }
    console.log(i);
}

// циклы нужны для того, чтоб выполнять один и тот же участок кода необходимое количество итераций
// например, если нам известно число повторений - лучше использовать цикл for
// если участок должен повторяться пока не выполнится условие, то while