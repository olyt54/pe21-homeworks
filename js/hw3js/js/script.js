"use strict";

let firstNumber = prompt("Enter your first number", ""),
    secondNumber = prompt("Enter your second number", ""),
    mathOperation = prompt("Enter math operation", "");
while (isNaN(firstNumber) || firstNumber == null || firstNumber === "") {
    firstNumber = prompt("Enter your first number", firstNumber);
}
while (isNaN(secondNumber) || secondNumber == null || secondNumber === "") {
    secondNumber = prompt("Enter your second number", secondNumber);
}

function calc(a, b, c) {
    let result = 0;
    switch (c) {
        case "+":
            result = a + b;
            break;
        case "-":
            result = a - b;
            break;
        case "*":
            result = a * b;
            break;
        case "/":
            result = a / b;
            break;
        default:
            alert("You not enter operation symbol!")
    }
    return result;
}

console.log(calc(+firstNumber, +secondNumber, mathOperation));


// функции нужны для того, чтоб содавать универсальные блоки кода, которые можно использовать повторно меняя её параметры (аргументы)
// аргументы помогают делать функцию гибкой и реиспользуемой. передавая разные аргументы можно получать различный результат используя один и тот же блок кода