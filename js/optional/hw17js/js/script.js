"use strict"

const student = {
    name: name,
    lastName: "last name",
}

student.name = prompt("Enter your name", "");
student.lastName = prompt("Enter your last name", "");

student.table = {};

function  dName(){
    let disciplineName = prompt("Enter discipline name", "");
    if(!disciplineName){
        return null;
    }
    let disciplineGrade = prompt("Enter the grade that you truly deserve", "");
    student.table[disciplineName] = disciplineGrade;
    return disciplineName;
}
do {
    dName()
} while ( dName() !== null);

function calcBadGrades () {
    let countGrades = 0;
    for (let key in student.table) {
        if (student.table[key] < 4) {
            countGrades++;
        }
    }
    if (countGrades === 0) {
        alert("Студент переведен на следующий курс")
    } else {
        console.log("Sorry, you're not a winner");
    }
}

console.log(calcBadGrades());

function calcAverageGrade () {
    let sum = 0;
    let countDisciplines = 0;
    for (let key in student.table) {
        sum += student.table[key];
        countDisciplines++;
    }
    return sum / countDisciplines;
}

console.log(calcAverageGrade());

