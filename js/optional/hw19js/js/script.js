"use strict";

function shallWeBeHurry(teamArr, taskArr, deadline) {
    const daysNeed = () => {
        let teamStoryPointPerDay = 0;
        for (let i = 0; i < teamArr.length; i++) {
            teamStoryPointPerDay += teamArr[i];
        }
        let allStoryPoint = 0;
        for (let i = 0; i < taskArr.length; i++) {
            allStoryPoint += taskArr[i];
        }
        return allStoryPoint / teamStoryPointPerDay;
    }
    const daysLeft = () => {
        let today = new Date();
        let workDays = 0;
        for (let i = today; i <= deadline; i.setDate(i.getDate() + 1)) {
            if (i.getDay() !== 0 || i.getDay() !== 6) {
                workDays++;
            }
        }
        return workDays;
    }
    if (daysNeed() <= daysLeft()) {
        console.log("You'll not be late");
    } else {
        console.log("You have not enough time");
    }
}

console.log(shallWeBeHurry([1, 1, 1, 1], [500, 500, 500, 500], new Date(2020, 8, 20)));