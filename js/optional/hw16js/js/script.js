"use strict";

let nFibonacci = +prompt("Enter number of Fibonacci row", "");

function calcFibonacci (n) {
    if (n <= 1) {
        return n;
    } else {
        return calcFibonacci(n - 2) + calcFibonacci(n - 1);
    }
}

alert(`Your number in Fibonacci sequences is ${calcFibonacci(nFibonacci)}`);