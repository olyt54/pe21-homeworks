"use strict"

let factorialNum = prompt("Enter your number, please", "");
while (factorialNum == null || isNaN(factorialNum) || factorialNum === "") {
    factorialNum = prompt("Just enter a number", "");
}

function calcFactorial(num) {
    if (num === 1) {
        return 1;
    } else {
        return num * calcFactorial(num - 1)
    }
}

document.write(calcFactorial(factorialNum));


// рекурсия в данном случае это когда функция вызывает сама себя, создается цепочка незаверщенных вложенностей,
// которые начнут завершаться и возвращать результат на уровень выше в тот момент, когда последний вложенный вызов
// вернет результат не требующий повторного вызова. я пытался попроще, но как-то так.