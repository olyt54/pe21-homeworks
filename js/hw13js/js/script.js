"use strict"

function changeTheme() {
    const sections = [...document.getElementsByTagName("section")],
        innerBgPopularSection = [...document.getElementsByClassName("popular-posts-item")],
        innerBgHotNews = [...document.querySelectorAll(".hot-news-section div")],
        content = [...document.getElementsByTagName("p")],
        heads = [...document.getElementsByClassName("item-head")];

    sections.forEach((item, index) => {
        item.classList = localStorage.getItem(`currentThemeBgSection-${index}`);
    });
    innerBgPopularSection.forEach((item, index) => {
        item.classList = localStorage.getItem(`currentThemeInnerBgPopular-${index}`);
    });
    innerBgHotNews.forEach((item, index) => {
        if (index !== 0) {
            item.classList = localStorage.getItem(`currentThemeInnerBgHot-${index}`);
        }
    });
    content.forEach((item, index) => {
        item.classList = localStorage.getItem(`currentThemeContent-${index}`);
    });
    heads.forEach((item, index) => {
        item.classList = localStorage.getItem(`currentThemeHeads-${index}`);
    });

    document.body.addEventListener("click", event => {
        if (event.target.classList.contains("theme-btn")) {
            sections.forEach((item, index) => {
                item.classList.toggle("section-bg-dark");
                localStorage.setItem(`currentThemeBgSection-${index}`, item.classList);
            });
            innerBgPopularSection.forEach((item, index) => {
                item.classList.toggle("local-bg-dark");
                localStorage.setItem(`currentThemeInnerBgPopular-${index}`, item.classList);
            });
            innerBgHotNews.forEach((item, index) => {
                if (index !== 0) {
                    item.classList.toggle("local-bg-dark");
                    localStorage.setItem(`currentThemeInnerBgHot-${index}`, item.classList);
                }
            });
            content.forEach((item, index) => {
                item.classList.toggle("text-color-dark");
                localStorage.setItem(`currentThemeContent-${index}`, item.classList);
            });
            heads.forEach((item, index) => {
                item.classList.toggle("text-color-dark");
                localStorage.setItem(`currentThemeHeads-${index}`, item.classList);
            });

        }
    })

}


function createThemeButton() {
    const themeBtn = document.createElement("button");
    themeBtn.classList.add("theme-btn");
    themeBtn.textContent = "Change color theme";
    document.body.prepend(themeBtn);
}
createThemeButton();
changeTheme();