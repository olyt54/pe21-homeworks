'use strict'

let name = prompt("How is your name?", '');
let age = +prompt("How old are you?", '');

while (name == null || isNaN(age)) {
    name = prompt("How is your name?", name);
    age = prompt("How old are you?", age);
}

function siteAcces() {
    if (age < 18) {
        alert("You are not allowed to visit this website");
    } else if (age >= 18 && age <= 22) {
        if (confirm("Are you sure you want to continue?") === true) {
            alert("Welcome, " + name);
        } else {
            alert("You are not allowed to visit this website");
        }
    } else {
        alert("Welcome, " + name);
    }
}
siteAcces();


//var имеет область видимости ограниченную функцией и переменную можно вызвать до её обьявления с значением undefined
//let ограничивается блоком и её нельзя вызвать до обьявления
//const ведёт себя так же как и let тем отличием, что значение переменной нельзя перезаписывать
//Плохой тон, потому что использование var может приводить к ошибкам, учитывая свойства этого способа обьявления