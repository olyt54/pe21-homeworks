"use strict"


function cycledImages() {
    const imgItems = [...document.getElementsByClassName('image-to-show')];

    imgItems.forEach(item => {
        if (item.dataset.index !== "1") {
            item.hidden = true;
        }
    });

    document.body.prepend(createBtns());

    let timer = setTimeout(function timerHandler() {
        let unhiddenImg = imgItems.find(item => {
            return !item.hidden;
        })
        imgItems.forEach(item => {
            if ((Number(unhiddenImg.dataset.index) + 1) === Number(item.dataset.index)) {
                unhiddenImg.hidden = true;
                item.hidden = false;
            } else if (unhiddenImg.dataset.index === "4" && item.dataset.index === "1") {
                unhiddenImg.hidden = true;
                item.hidden = false;
            }
        });
        timer = setTimeout(timerHandler, 10000);

        document.body.addEventListener("click", event => {
            if (event.target.dataset.action === "stop") {
                clearTimeout(timer);
            } else if (event.target.dataset.action === "continue") {
                timer = setTimeout(timerHandler, 10000);
            }
        })
    }, 10000);
}

function createBtns() {
    const buttonsWrapper = document.createElement("div"),
        stopButton = document.createElement("button"),
        continueButton = document.createElement("button");

    buttonsWrapper.classList.add("btn-wrapper");
    stopButton.classList.add("btn");
    continueButton.classList.add("btn");

    stopButton.dataset.action = "stop";
    continueButton.dataset.action = "continue";

    stopButton.textContent = "Прекратить";
    continueButton.textContent = "Возобновить показ";

    buttonsWrapper.append(stopButton, continueButton);

    return buttonsWrapper;
}
cycledImages();