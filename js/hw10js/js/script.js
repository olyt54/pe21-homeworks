"use strict"

function showPassword() {
    const inputPass = document.getElementsByTagName("input")[0],
        inputConfirmPass = document.getElementsByTagName("input")[1],
        passForm = document.getElementsByClassName("password-form")[0],
        btnVisibility = document.getElementsByClassName("icon-password"),
        btnSubmit = document.getElementsByClassName("btn")[0],
        errorMessage = document.createElement("span");

    btnVisibility[1].classList.remove("fa-eye-slash");
    btnVisibility[1].classList.add("fa-eye");
    errorMessage.classList.add("error-message");

    errorMessage.textContent = "Нужно ввести одинаковые значения";

    passForm.addEventListener("click", e => {
        switch (e.target) {
            case btnVisibility[0]:
                btnVisibility[0].classList.toggle("fa-eye-slash");
                btnVisibility[0].classList.toggle("fa-eye");

                if (inputPass.type === "password") {
                    inputPass.type = "text";
                } else {
                    inputPass.type = "password";
                }
                break
            case btnVisibility[1]:
                btnVisibility[1].classList.toggle("fa-eye-slash");
                btnVisibility[1].classList.toggle("fa-eye");

                if (inputConfirmPass.type === "password") {
                    inputConfirmPass.type = "text";
                } else {
                    inputConfirmPass.type = "password";
                }
                break
            case btnSubmit:
                if (inputPass.value !== "" && inputConfirmPass.value !== "" && inputPass.value === inputConfirmPass.value) {
                    alert("You are welcome!");
                    console.log(passForm);
                    if (passForm.contains(errorMessage)) {
                        passForm.removeChild(errorMessage);
                    }

                } else if (inputPass.value !== inputConfirmPass.value) {
                    passForm.insertBefore(errorMessage, btnSubmit);
                } break
        }
    })
}

showPassword();