"use strict"

// our services section

const ourServicesDataArray = [
    {
        tab: "first",
        imgUrl: "./img/services-section/web.png",
        alt: "Drawing on graphic pad",
        content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    },
    {
        tab: "second",
        imgUrl: "./img/services-section/graphic.jpg",
        alt: "Work table with color templates",
        content: "Accusantium ad amet asperiores, blanditiis cum delectus distinctio dolor dolore dolorem ea ex explicabo fugit hic illo in iusto labore laborum, molestias mollitia neque nostrum odio omnis optio perferendis placeat porro quas qui recusandae rem sapiente soluta suscipit temporibus ullam vero vitae voluptate voluptatibus? Adipisci amet blanditiis dignissimos dolor facilis, minus nam numquam possimus quia saepe, sed voluptatum. Ab aliquid at atque consequatur corporis culpa cumque debitis dicta ducimus eaque facilis illo in itaque iure, iusto labore laudantium libero mollitia neque nihil placeat porro, quas quia reiciendis rem repellendus sed similique sint suscipit tempore veritatis vitae voluptatem voluptatibus?"
    },
    {
        tab: "third",
        imgUrl: "./img/services-section/support.jpg",
        alt: "Support key on keyboard",
        content: "Accusamus at dolorem eaque earum esse excepturi facere fuga fugiat fugit id illo in ipsam minus molestias mollitia nam nisi nobis non nostrum nulla quae, quaerat quam quibusdam recusandae rem repellat reprehenderit repudiandae sequi tenetur ullam unde voluptatem voluptates voluptatibus? Aperiam fuga, impedit iure laboriosam nisi non nostrum recusandae sequi. At dignissimos labore quos recusandae reiciendis sint? At consequatur cumque dolorem facere magni modi natus necessitatibus, nemo nihil non possimus, quisquam repellendus sed. Delectus dolores, ducimus eaque eius eos est et ex, id ipsam ipsum laboriosam minus molestiae molestias odio pariatur quasi quibusdam sunt. Animi dolorum est maxime nam placeat porro repudiandae saepe sunt."
    },
    {
        tab: "fourth",
        imgUrl: "./img/services-section/app.jpg",
        alt: "Mobile app design template",
        content: "Ab accusamus ad adipisci alias aspernatur aut cupiditate dolore dolorem doloremque ducimus eveniet explicabo incidunt laborum libero minus nihil nisi odio officiis praesentium provident quaerat reiciendis, reprehenderit similique sint tempora tempore, totam velit? Animi at cupiditate ea explicabo facere impedit magni neque, numquam repellendus! Debitis dignissimos dolorum, error, eveniet fuga id libero molestiae quae quibusdam quod reiciendis reprehenderit, ut. A at dicta eaque eius explicabo iste minus modi nam numquam perferendis quae quas repellat sequi voluptatibus, voluptatum. Aut dolorum excepturi ipsum minima, natus optio pariatur perferendis quibusdam repellat repellendus?"
    },
    {
        tab: "fifth",
        imgUrl: "./img/services-section/marketing.jpg",
        alt: "Person from monitor selling you stuff",
        content: "Accusamus animi assumenda consequuntur deserunt dolorem eius, inventore, ipsum iste iusto minus, neque obcaecati officia perspiciatis provident reiciendis sed sequi similique temporibus vel vitae. Alias aperiam asperiores aspernatur at blanditiis consectetur culpa dolores ea est eveniet fugit hic in, ipsa ipsum iste laborum minus modi necessitatibus nostrum, odit perspiciatis sapiente, similique tempora tenetur vel velit veniam vero voluptas voluptatem voluptates? Culpa dicta, dignissimos eius enim ipsam magni praesentium similique! Eum exercitationem inventore provident? Assumenda cum dolor dolore exercitationem facilis hic illo ipsam laborum, natus nisi perferendis qui, quis quisquam sapiente unde."
    },
    {
        tab: "sixth",
        imgUrl: "./img/services-section/seo.jpg",
        alt: "Abstract image about seo",
        content: "Accusantium alias aliquam atque aut deserunt distinctio dolore doloremque ducimus error esse eveniet expedita explicabo facere, illum inventore libero maiores maxime minima odit, optio perferendis quaerat quasi quo reiciendis repellendus sed ullam. Architecto beatae, dolores eveniet itaque labore magnam maiores omnis praesentium quaerat, quas quo repellat repudiandae, ut vitae voluptas. Cumque dolor dolorem, doloribus ea eius, ipsam iste itaque quam quis soluta tempora voluptatum. Consectetur dignissimos est impedit labore natus obcaecati perferendis quae suscipit totam unde. Labore, quas quo."
    }
]

function showActiveTab(dataArray) {
    const tabItems = document.getElementsByClassName("tab-list")[0],
        tabImage = document.getElementById("tab-image"),
        tabContent = document.getElementById("tab-content");

    tabImage.src = dataArray[0].imgUrl;
    tabImage.alt = dataArray[0].alt;
    tabContent.textContent = dataArray[0].content;

    tabItems.addEventListener("click", event => {
        if (event.target.classList.contains("active-tab")) {
            return null;
        } else {
            const currentObject = dataArray.find(item => {
                if (event.target.dataset.item === item.tab) {
                    return item;
                }
            });

            [...tabItems.children].find(item => {
                return item.classList.contains("active-tab");
            }).classList.remove("active-tab");

            event.target.classList.add("active-tab");

            tabImage.src = currentObject.imgUrl;
            tabImage.alt = currentObject.alt;
            tabContent.textContent = currentObject.content;
        }
    })
}

showActiveTab(ourServicesDataArray);

// amazing work section
const amazingImg = [...document.getElementsByClassName("amazing-work-item")];

function loadImg(sourceImgArr) {
    const loadBtn = document.getElementById("amazing-load-btn");

    sourceImgArr.forEach((item, index) => {
        if (index >= 12) {
            item.classList.add("amazing-work-hidden");
        }
    })

    let clicksCount = 0,
        displayedImg = sourceImgArr.filter(item => {
            if (item.classList.contains("amazing-work-hidden") === false) {
                return item;
            }
        });
    loadBtn.addEventListener("click", event => {
        const tabActive = document.getElementsByClassName("amazing-active-tab")[0];
        if (tabActive.dataset.category !== "all") {
            sourceImgArr.forEach(item => {
                if (item.dataset.category === tabActive.dataset.category) {
                    item.classList.remove("amazing-work-hidden");
                    displayedImg.push(item);
                } else {
                    item.classList.add("amazing-work-hidden");
                }
            });
            sortImg(displayedImg);
        } else {
            clicksCount += 1;
            if (clicksCount === 1) {
                sourceImgArr.forEach((item, index) => {
                    if (index >= 24) {
                        item.classList.add("amazing-work-hidden");
                    } else {
                        item.classList.remove("amazing-work-hidden");
                        displayedImg.push(item);
                    }
                });
                sortImg(displayedImg);
            } else if (clicksCount >= 2) {
                sourceImgArr.forEach((item, index) => {
                    item.classList.remove("amazing-work-hidden");
                    displayedImg.push(item);
                });
                sortImg(displayedImg);
                loadBtn.remove();
            }
        }
    });
    return displayedImg;
}

function sortImg(imgArray) {
    const tabs = document.getElementsByClassName("amazing-tab-list")[0];

    tabs.addEventListener("click", event => {
        [...tabs.children].find(item => {
            return item.classList.contains("amazing-active-tab");
        }).classList.remove("amazing-active-tab");

        event.target.classList.add("amazing-active-tab");

        imgArray.forEach(item => {
            if (event.target.dataset.category === item.dataset.category) {
                item.classList.remove("amazing-work-hidden");
            } else if (event.target.dataset.category === "all") {
                item.classList.remove("amazing-work-hidden");
            } else {
                item.classList.add("amazing-work-hidden");
            }
        })
    })

}
sortImg(loadImg(amazingImg));

//slider

const topSliderContainer = document.querySelector(".top-slider"),
    lowerSliderContainer = document.querySelector(".lower-slider");

let topSlider = new Swiper(topSliderContainer, {

    loop: true,
    loopedSlides: 5, //looped slides should be the same
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});


let lowerSlider = new Swiper(lowerSliderContainer, {
    spaceBetween: 25,
    slidesPerView: 4,
    loop: true,
    loopedSlides: 5,
    watchSlidesVisibility: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

document.addEventListener("keydown", event => {
    if (event.key === "ArrowRight") {
        lowerSlider.slideNext();
    }
})
document.addEventListener("keydown", event => {
    if (event.key === "ArrowLeft") {
        lowerSlider.slidePrev();
    }
})
document.addEventListener("keydown", event => {
    if (event.key === "ArrowRight") {
        topSlider.slideNext();
    }
})
document.addEventListener("keydown", event => {
    if (event.key === "ArrowLeft") {
        topSlider.slidePrev();
    }
})

// masonry

let elem = document.querySelector(".grid"),
    msnry = new Masonry(elem, {
        itemSelector: ".grid-item",
        columnWidth: 1,
        percentPosition: true,
        gutter: 15,
        //сперва я сделал grid-sizer c шириной calc((100% / 3) / 3), что вроде как логично
        //потому что 3 колонки, в каждой из которых может быть 3 картинки
        //но это не работало правильно, он в некоторых местах оставлял пустое место
        //поэтому я написал так - уродливо по отступам, но не оставляет пустых мест
    });

msnry.layout();



function createGridItem() {
    const gridItem = document.createElement("div"),
        itemImg = document.createElement("img"),
        sourceHoverItem = document.getElementsByClassName("grid-item-hover")[0];

    const newHoverItem = sourceHoverItem.cloneNode(true);

    gridItem.classList.add("grid-item");
    itemImg.classList.add("grid-item-img");

    gridItem.append(itemImg);
    gridItem.append(newHoverItem);

    return gridItem;
}

const secondLoadImg = [
    "./img/best-images-section/second-load/1.png",
    "./img/best-images-section/second-load/2.png",
    "./img/best-images-section/second-load/3.png",
    "./img/best-images-section/second-load/4.png",
    "./img/best-images-section/second-load/5.png",
    "./img/best-images-section/second-load/6.png",
    "./img/best-images-section/second-load/7.png",
    "./img/best-images-section/second-load/8.png",
    "./img/best-images-section/second-load/9.png",
    "./img/best-images-section/second-load/10.png",
    "./img/best-images-section/second-load/11.png",
    "./img/best-images-section/second-load/12.png",
    "./img/best-images-section/second-load/13.png",
    "./img/best-images-section/second-load/14.png",
    "./img/best-images-section/second-load/15.png",
    "./img/best-images-section/second-load/16.png",
    "./img/best-images-section/second-load/17.png",
    "./img/best-images-section/second-load/18.png",
],
    thirdLoadImg = [
        "./img/best-images-section/third-load/1.png",
        "./img/best-images-section/third-load/2.png",
        "./img/best-images-section/third-load/3.png",
        "./img/best-images-section/third-load/4.png",
        "./img/best-images-section/third-load/5.png",
        "./img/best-images-section/third-load/6.png",
        "./img/best-images-section/third-load/7.png",
        "./img/best-images-section/third-load/8.png",
        "./img/best-images-section/third-load/9.png",
        "./img/best-images-section/third-load/10.png",
        "./img/best-images-section/third-load/11.png",
        "./img/best-images-section/third-load/12.png",
        "./img/best-images-section/third-load/13.png",
        "./img/best-images-section/third-load/14.png",
        "./img/best-images-section/third-load/15.png",
        "./img/best-images-section/third-load/16.png",
        "./img/best-images-section/third-load/17.png",
        "./img/best-images-section/third-load/18.png",
    ];

function createReadyItem(imgArr) {
    const gridItems = imgArr.map(item => {
        const gridItem = createGridItem();
        //сперва я написал блок который ищет именно тег img
        //но по логике разметки и так понятно, что он всегда будет первым, так что так намного короче
        gridItem.children[0].src = item;
        gridItem.children[0].alt = "Random lovely picture";
        return gridItem;
    });

    return gridItems;
}

function loadGridItems(btnId) {
    const loadBtn = document.getElementById(btnId),
        gridContainer = document.getElementsByClassName("grid")[0],
        loadAnimation = document.getElementsByClassName("load")[0];

    let clickCounts = 0;

    loadBtn.addEventListener("click", event => {
        clickCounts += 1;

        if (clickCounts === 1) {
            let newContent = createReadyItem(secondLoadImg);

            loadAnimation.classList.toggle("load-visible");

            setTimeout(() => {
                loadAnimation.classList.toggle("load-visible");

                newContent.forEach(item => {
                    gridContainer.append(item);
                })
                msnry.appended(newContent);
                msnry.layout();
            }, 2000);
        } else if (clickCounts >= 2) {
            let newContent = createReadyItem(thirdLoadImg);

            loadAnimation.classList.toggle("load-visible");

            setTimeout(() => {
                loadAnimation.classList.toggle("load-visible");

                newContent.forEach(item => {
                    gridContainer.append(item);
                })
                msnry.appended(newContent);
                msnry.layout();
                loadBtn.remove();
            }, 2000);
        }
    })
}

loadGridItems("best-images-btn");
